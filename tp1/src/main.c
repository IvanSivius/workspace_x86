/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int barreraAbierta = 0;
    int esperar = 0;
    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        
        
        
        input = hw_LeerEntrada();
        
        if (!barreraAbierta && input == SENSOR_1) {
            hw_AbrirBarrera();
            barreraAbierta = 1;
        }

        if(!barreraAbierta && input == SENSOR_2)
        {
            printf("SUENA ALARMA");
            barreraAbierta = 0;
        }    

        if (barreraAbierta && input == SENSOR_2) {
            
            
            while(esperar < 5){
                printf("%d seg de espera\n", &esperar);
                hw_Pausems(1000);
                
                input = hw_LeerEntrada();
                if(input ==  SENSOR_1){
                    esperar = 0;
                    printf("Se detectó otro auto\n");
                }else{
                    esperar++;
                }
            }
            esperar = 0;
            barreraAbierta = 0;
            hw_CerrarBarrera();
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
