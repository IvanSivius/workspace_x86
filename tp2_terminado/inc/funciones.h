#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#define ESPERA_DE_SELECCION 30
#define ESPERA_DE_CAFE 45
#define ESPERA_DE_TE 30
#define ESPERA_DE_ALARMA 2

typedef enum {
    REPOSO,
    ESPERANDO_SELECCION,
    PREPARANDO_CAFE,
    PREPARANDO_TE,
    SIRVIENDO
} MS_EXPENDEDORA;

void ms_expendedora_init(void);
void ms_expendedora(void);
void clear_events(void);
void cambiarEstadoLED(void);
bool acEstadoLed(void);
int frecLED(void);
void establecerFrecuencia(int f);
void evFicha_ON(void);
void evCafe_ON(void);
void evTe_ON(void);
void evTick500ms_ON(void);
void evTick1seg_ON(void);
void evTick100ms_ON(void);
bool mostrarLED_on();
void toggleLED(void);

#endif // FUNCIONES_H_INCLUDED