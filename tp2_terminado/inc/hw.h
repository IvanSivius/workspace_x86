#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stdbool.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define SENSOR_1  49  // ASCII para la tecla 1
#define SENSOR_2  50  // ASCII para la tecla 2
#define SENSOR_3  51   //ASCII para la tecla 3


// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_DevolverMoneda(void);
void hw_PreparandoCafe(void);
void hw_PreparandoTe(void);
void hw_teTerminado(void);
void hw_cafeTerminado(void);
void hw_ActivarAlarma(void);
void hw_ApagarAlarma(void);


#endif