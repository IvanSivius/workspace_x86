#include "funciones.h"
#include "hw.h"



MS_EXPENDEDORA state;

bool mostrarLED;
bool estadoLED;
bool evFicha;
bool evTe;
bool evCafe;
bool evTick1seg_raised;
bool evTick100ms_raised;
bool evTick500ms_raised;
uint8_t count_seg;
uint8_t count_led;


void ms_expendedora_init(void)
{
    state = REPOSO;
    mostrarLED = true;
    estadoLED = true;
    clear_events();

}

void clear_events(void)
{
    evFicha = 0;
    evTe = 0;
    evCafe = 0;
    evTick1seg_raised = 0;
    evTick500ms_raised = 0;
    evTick100ms_raised = 0;
}

void ms_expendedora(void)
{

    switch(state)
    {
    case REPOSO:
                    
        if(evFicha)
        {
            count_seg = 0;
            state = ESPERANDO_SELECCION;
        }
        else if(evTick500ms_raised)
        {
            toggleLED();
        }
        
        break;

    case ESPERANDO_SELECCION:

        if(evCafe)
        {
            state = PREPARANDO_CAFE;
            count_seg = 0;
        }
        else if(evTe)
        {

            state = PREPARANDO_TE;
            count_seg = 0;

        }
        else if(evTick1seg_raised && count_seg < ESPERA_DE_SELECCION)
        {
            count_seg++;
        }
        else if (evTick1seg_raised && count_seg == ESPERA_DE_SELECCION)
        {
            hw_DevolverMoneda();
            state = REPOSO;
            count_seg = 0;
        }

        break;

    case PREPARANDO_CAFE:
            
        if(evTick1seg_raised && count_seg < ESPERA_DE_CAFE)
        {
            hw_PreparandoCafe();
            count_seg++;
        }
        else if (evTick1seg_raised && count_seg == ESPERA_DE_CAFE)
        {
            hw_cafeTerminado();
            state = SIRVIENDO;
            count_seg = 0;
        }
        else if(evTick100ms_raised)
        {
            toggleLED();
        }
        break;

    case PREPARANDO_TE:
       
        if(evTick1seg_raised && count_seg < ESPERA_DE_TE)
        {
            hw_PreparandoTe();
            count_seg++;
        }
        else if (evTick1seg_raised && count_seg == ESPERA_DE_TE)
        {
            hw_teTerminado();
            state = SIRVIENDO;
        }
        else if(evTick100ms_raised)
        {
            toggleLED();
        }
    
        break;

    case SIRVIENDO:
        if(evTick1seg_raised && count_seg < ESPERA_DE_ALARMA)
        {
            hw_ActivarAlarma();
            count_seg++;
        }
        else if (evTick1seg_raised && count_seg == ESPERA_DE_ALARMA)
        {
            state = REPOSO;
            hw_ApagarAlarma();
            count_seg = 0;
        }
        break;
    }
    clear_events();

}

void evFicha_ON(void)
{
    evFicha = 1;
}

void evCafe_ON(void)
{
    evCafe = 1;
}

void evTe_ON(void)
{
    evTe = 1;
}

void evTick1seg_ON(void)
{
    evTick1seg_raised = 1;
}
void evTick100ms_ON(void)
{
    evTick100ms_raised = 1;
}
void evTick500ms_ON(void)
{
    evTick500ms_raised = 1;
}
void toggleLED()
{
    //ESTA FUNCION CAMBIA EL ESTADO DEL LED
    //printf("cambiando estado led\n");
}