#include "funciones.h"
#include "hw.h"
#include "main.h"
#include <stdio.h>

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;
    uint16_t cont_100ms = 0;

    hw_Init();

    ms_expendedora_init();

    printf("PASO 1: Ficha presione 1\n\n");
    printf("PASO 2: CAFE : 2  -- Te : 3\n\n");

    while (input != EXIT)
    {
        input = hw_LeerEntrada();

        if (input == SENSOR_1)
        {
            evFicha_ON();
        }

        if (input == SENSOR_2)
        {
            evCafe_ON();
        }
        else if (input == SENSOR_3)
        {
            evTe_ON();
        }

        cont_ms++;

        if (cont_ms == 100)
        {
            cont_ms = 0;
            evTick100ms_ON();
            cont_100ms++;
            if(cont_100ms == 5)
            {
                evTick500ms_ON();
            }
            if (cont_100ms == 10)
            {
                cont_100ms = 0;
                evTick1seg_ON();
            }
        }

        ms_expendedora();

        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}