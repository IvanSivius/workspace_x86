
#include <hw.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>


struct termios oldt, newt;

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_DevolverMoneda(void)
{
    printf("Devolver Moneda\n\n");
}

void hw_PreparandoCafe(void)
{
    printf("Preparando Cafe\n\n");
}

void hw_PreparandoTe(void)
{
    printf("Preparando Te\n\n");
}

void hw_teTerminado(void)
{
    printf("El TE esta listo\n\n");
}
void hw_cafeTerminado(void)
{
    printf("El cafe esta listo\n\n");
}


void hw_ActivarAlarma(void)
{
    printf("Alarma activada \n\n");
}

void hw_ApagarAlarma(void)
{
    printf("Alarma apagada \n\n");
}

/*==================[end of file]============================================*/