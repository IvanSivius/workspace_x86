#ifndef _GARAJE_H_
#define _GARAJE_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    REPOSO,
    INGRESANDO,
    ESPERANDO_EGRESO,
    ALARMA
} FSM_GARAJE_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_garaje_init(void);
void fsm_garaje_runCycle(void);

// Eventos
void fsm_garaje_raise_evSensor1_On(void);
void fsm_garaje_raise_evSensor2_On(void);
void fsm_garaje_raise_evSensor2_Off(void);
void fsm_garaje_raise_evTick1seg(void);

// Debugging
void fsm_garaje_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _GARAJE_H_ */
